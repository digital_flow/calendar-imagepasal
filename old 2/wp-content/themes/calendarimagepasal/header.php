<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Mirrored from themes.semicolonweb.com/html/canvas/demo-furniture.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Nov 2021 09:15:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="ImagePasal" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="preconnect" href="https://fonts.gstatic.com/">

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;family=Zilla+Slab:wght@400;500&amp;display=swap" rel="stylesheet">
    
    <?php wp_head(); ?>

</head>


<body class="stretched <?php if(is_front_page() ){ ?> home-page <?php } ?>">

    <!-- Cart Panel Background
    ============================================= -->
    <div class="body-overlay"></div>

    <!-- Cart Side Panel
    ============================================= -->
    <div id="side-panel" class="bg-white">

        <!-- Cart Side Panel Close Icon
        ============================================= -->
        <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

        <div class="side-panel-wrap">

            <div class="top-cart d-flex flex-column h-100">
                <div class="top-cart-title">
                    <h4>Shopping Cart <small class="bg-color-light icon-stacked text-center rounded-circle color"><?= WC()->cart->get_cart_contents_count() ?></small></h4>
                </div>

                <!-- Cart Items
                ============================================= -->
                <div class="top-cart-items">

                    <?php woocommerce_mini_cart(); ?>

                    <!-- Cart Item 1
                    ============================================= -->
                    <div class="top-cart-item d-none">
                        <div class="top-cart-item-image border-0">
                            <a href="#"><img src="demos/furniture/images/cart/1.jpg" alt="Cart Image 1" /></a>
                        </div>
                        <div class="top-cart-item-desc">
                            <div class="top-cart-item-desc-title">
                                <a href="#" class="fw-medium">Blue Sofa for Dining Room</a>
                                <span class="top-cart-item-price d-block"><del>$29.99</del> $19.99</span>
                                <div class="d-flex mt-2">
                                    <a href="#" class="fw-normal text-black-50 text-smaller"><u>Edit</u></a>
                                    <a href="#" class="fw-normal text-black-50 text-smaller ms-3"><u>Remove</u></a>
                                </div>
                            </div>
                            <div class="top-cart-item-quantity">x 1</div>
                        </div>
                    </div>

          
                </div>

        

                <!-- Cart Price and Button
                ============================================= -->
                <div class="top-cart-action flex-column align-items-stretch d-none">
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <small class="text-uppercase ls1">Total</small>
                        <h4 class="fw-medium font-body mb-0">$69.97</h4>
                    </div>
                    <a href="#" class="button btn-block text-center m-0 fw-normal"><i style="position: relative; top: -2px;"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="#FFF" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><path d="M40,192a16,16,0,0,0,16,16H216a8,8,0,0,0,8-8V88a8,8,0,0,0-8-8H56A16,16,0,0,1,40,64Z" opacity="0.2"></path><path d="M40,64V192a16,16,0,0,0,16,16H216a8,8,0,0,0,8-8V88a8,8,0,0,0-8-8H56A16,16,0,0,1,40,64v0A16,16,0,0,1,56,48H192" fill="none" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="10"></path><circle cx="180" cy="144" r="12"></circle></svg></i> Checkout</a>
                </div>
            </div>

        </div>

    </div>

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Top Bar
        ============================================= -->
        <div id="top-bar" class="py-1 text-center bg-light">
            <div class="container clearfix">
                <div class="d-md-flex justify-content-md-between align-items-md-center">

                        <?php $top_info = get_field('top_info', 'option'); ?>


                   <p class="my-2 m-md-0  fw-normal text-muted"> <?= $top_info['info1'] ?> 
                        <?php if($top_info['info2']) { ?>
                                <span class="mx-2 text-black-50">&middot;</span> 
                                    <?php echo $top_info['info2']; } ?> 
                        <?php if($top_info['info3']) { ?>
                                <span class="mx-2 text-black-50">&middot;</span>
                                    <?php echo $top_info['info3']; } ?>
                    </p>

                    <?php $contact = get_field('contact', 'option'); ?>
                   
                   <p class="mb-0  fw-normal text-muted d-none d-md-block">

                    Need Help? Call us at 
                    <a class="" href="tel:<?= $contact['phone_number'] ?>"> <?= $contact['phone_number'] ?></a> 

                    or email us :
                        <a class="" href="mailto:<?= $contact['email'] ?>"><?= $contact['email'] ?></a></p>

                </div>
            </div>
        </div>

        <!-- Header
        ============================================= -->
        <header id="header" class="header-size-sm border-bottom-0">
            <div id="header-wrap">
                <div class="container">
                    <div class="header-row justify-content-lg-between">

                        <!-- Logo
                        ============================================= -->
                        <div id="logo" class="  flex-column ">
                            <a href="<?php echo get_home_url(); ?>" class="standard-logo"><img src="<?= wp_get_attachment_image_url(get_theme_mod('custom_logo'), 'full') ?>" alt="Canvas Logo" style="height: 21px;"></a>
                            <a href="<?php echo get_home_url(); ?>" class="retina-logo"><img src="<?= wp_get_attachment_image_url(get_theme_mod('custom_logo'), 'full') ?>" alt="Canvas Logo" style="height: 14px;"></a>
                        </div>
                        <!-- #logo end -->

                        <div class="header-misc col-auto col-lg-3  justify-content-lg-end ms-0 ms-sm-3 px-0">

                            <!-- Top Search
                            ============================================= -->
                            <div id="top-search" class="header-misc-icon">
                                <a href="#" id="top-search-trigger">
                                    <i><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="115.99512" cy="116" r="84" opacity="0.2"></circle><circle cx="115.99512" cy="116" r="84" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></circle><line x1="175.38868" y1="175.40039" x2="223.98926" y2="224.00098" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg></i>
                                    <i><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" opacity="0.2"></circle><circle cx="128" cy="128" r="96" fill="none" stroke="var(--themecolor)" stroke-miterlimit="10" stroke-width="16"></circle><line x1="160" y1="96" x2="96" y2="160" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="160" y1="160" x2="96" y2="96" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg></i>
                                </a>
                            </div><!-- #top-search end -->

                            <!-- Top Cart
                            ============================================= -->
                            <div id="top-cart" class="header-misc-icon">
                                 
                                <a href="<?= wc_get_cart_url() ?>" class="side-panel-trigger">
                                     <i class="icon-line-shopping-cart"></i>
                                    <span class="top-cart-number"><?= WC()->cart->get_cart_contents_count() ?></span>

                                </a>
                            </div>
                            <!-- #top-cart end -->

                        </div>

                        <!-- Mobile Menu Icon
                        ============================================= -->
                        <div id="primary-menu-trigger">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 256 256"><defs><style>.a,.c{fill:none;}.b{fill:var(--themecolor);opacity:0.2;}.c,.d{stroke:var(--themecolor);}.c{stroke-miterlimit:10;stroke-width:14px;}.d{stroke-linecap:round;stroke-linejoin:round;stroke-width:13px;}</style></defs><rect class="a" width="24" height="24"/><circle class="b" cx="96" cy="96" r="96" transform="translate(32 32)"/><circle class="c" cx="96" cy="96" r="96" transform="translate(32 32)"/><line class="d" x2="85" transform="translate(86 127)"/><line class="d" x2="85" transform="translate(86 97)"/><line class="d" x2="85" transform="translate(86 159)"/></svg>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" opacity="0.2"></circle><circle cx="128" cy="128" r="96" fill="none" stroke="var(--themecolor)" stroke-miterlimit="10" stroke-width="16"></circle><line x1="160" y1="96" x2="96" y2="160" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="160" y1="160" x2="96" y2="96" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>
                        </div>

                        <!-- Primary Navigation
                        ============================================= -->
                        <nav class="primary-menu with-arrows px-0 mx-auto">

                            <ul class="menu-container">

                                <?php 
                            wp_nav_menu(
                                array(
                                    'menu' => 'header',
                                    'container' => '',
                                     'add_li_class'  => 'menu-item',
                                     'add_a_class'     => 'menu-link',
                                    'items_wrap' => '<li> %3$s</li> '
                                )
                            );
                        ?>
                              
                            </ul>

                        </nav><!-- #primary-menu end -->

                        <!-- Top Search Form
                        ============================================= -->
                       <form class="top-search-form" action="<?= get_home_url() ?>" method="get">
                            <input type="text" name="s" id="s" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
                            <input type="hidden" name="post_type" id="post_type" value="product">
                        </form>

                    </div>
                </div>
            </div>
            <div class="header-wrap-clone"></div>
        </header><!-- #header end -->




