	<?php

get_header();
?>

<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<!-- Posts
					============================================= -->
					<div id="posts" class="post-grid row grid-container gutter-40 clearfix" data-layout="fitRows">

						<div class="entry col-md-4 col-sm-6 col-12">
							<div class="grid-inner">
								<div class="entry-image">
									<a href="@" data-lightbox="image"><img src="assets/images/hero-image.jpg" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-title">
									<h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><i class="icon-calendar3"></i> 10th Feb 2021</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
								</div>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, voluptatem, dolorem animi nisi autem blanditiis enim culpa reiciendis et explicabo tenetur!</p>
									<a href="blog-single.html" class="more-link">Read More</a>
								</div>
							</div>
						</div>
						<div class="entry col-md-4 col-sm-6 col-12">
							<div class="grid-inner">
								<div class="entry-image">
									<a href="@" data-lightbox="image"><img src="assets/images/hero-image.jpg" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-title">
									<h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><i class="icon-calendar3"></i> 10th Feb 2021</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
								</div>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, voluptatem, dolorem animi nisi autem blanditiis enim culpa reiciendis et explicabo tenetur!</p>
									<a href="blog-single.html" class="more-link">Read More</a>
								</div>
							</div>
						</div>
						<div class="entry col-md-4 col-sm-6 col-12">
							<div class="grid-inner">
								<div class="entry-image">
									<a href="@" data-lightbox="image"><img src="assets/images/hero-image.jpg" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-title">
									<h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><i class="icon-calendar3"></i> 10th Feb 2021</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
								</div>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, voluptatem, dolorem animi nisi autem blanditiis enim culpa reiciendis et explicabo tenetur!</p>
									<a href="blog-single.html" class="more-link">Read More</a>
								</div>
							</div>
						</div>
						<div class="entry col-md-4 col-sm-6 col-12">
							<div class="grid-inner">
								<div class="entry-image">
									<a href="@" data-lightbox="image"><img src="assets/images/hero-image.jpg" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-title">
									<h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><i class="icon-calendar3"></i> 10th Feb 2021</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
								</div>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, voluptatem, dolorem animi nisi autem blanditiis enim culpa reiciendis et explicabo tenetur!</p>
									<a href="blog-single.html" class="more-link">Read More</a>
								</div>
							</div>
						</div>
						<div class="entry col-md-4 col-sm-6 col-12">
							<div class="grid-inner">
								<div class="entry-image">
									<a href="@" data-lightbox="image"><img src="assets/images/hero-image.jpg" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-title">
									<h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
								</div>
								<div class="entry-meta">
									<ul>
										<li><i class="icon-calendar3"></i> 10th Feb 2021</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
								</div>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, voluptatem, dolorem animi nisi autem blanditiis enim culpa reiciendis et explicabo tenetur!</p>
									<a href="blog-single.html" class="more-link">Read More</a>
								</div>
							</div>
						</div>


					</div><!-- #posts end -->

					<div class="clear mt-5"></div>

					<!-- Pagination
					============================================= -->
					<div class="d-flex justify-content-between mt-5">
						<a href="#" class="btn btn-outline-secondary">&larr; Older</a>
						<a href="#" class="btn btn-outline-dark">Newer &rarr;</a>
					</div>
					<!-- .pager end -->

				</div>
			</div>
		</section><!-- #content end -->


	<?php

get_footer();
?>