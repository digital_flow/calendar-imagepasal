<!-- Hero Section
        ============================================= -->
        <section id="slider" class="slider-element  swiper_wrapper mb-5  ">
            <div class="slider-inner">

                <div class="swiper-container swiper-parent">
                    <div class="swiper-wrapper">
<?php
        while (have_rows('slides')):
            the_row();
            $slide_image = get_sub_field('image');
           
            ?>
            
                        <div class="swiper-slide dark h-100">
                            <div class="container">
                                <div class="row align-items-end justify-content-center justify-content-xl-start py-6 h-100">
                                    <div class="col-xl-6 col-lg-9 col-md-10 mt-xl-4  text-xl-start">
                                        <p class="op-07 text-white mb-3 text-uppercase ls2 text-smaller d-none"><?php the_sub_field('heading'); ?></p>
                                        <h1 class="display-4 mb-3 text-white fw-medium"><?php the_sub_field('heading'); ?></h1>
                                        <a href="<?php the_sub_field('button_link'); ?>" class="button button-primary "><i style="position: relative; top: -2px;"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><rect x="32" y="48" width="192" height="160" rx="8" opacity="0.2"></rect><rect x="32" y="48" width="192" height="160" rx="8" stroke-width="16" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" fill="none"></rect><path d="M168,88a40,40,0,0,1-80,0" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path></svg></i> <?php the_sub_field('button_text'); ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide-bg" style="background-image: url('<?php echo $slide_image; ?>');"></div>
                        </div>
 

                            <?php  
                                endwhile;
                                ?>

                    </div>
                    <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
                    <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
                </div>

                

            </div>
        </section>



