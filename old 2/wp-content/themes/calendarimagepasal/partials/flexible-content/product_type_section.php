	<!-- 3. Section product by
				============================================= -->
				<div class="section my-0 bg-transparent">
					<div class="container">
						<div class="text-center  mb-5">
							<h2 class="h1 fw-normal mb-4"><?php the_sub_field('heading');?></h2>
							
						</div>

 <?php 
                            $i = 1;
                             while (have_rows('product_type')):
                                the_row();
                                ?>
                       
						<div class="row align-items-center">
                            <div class="col-lg-3 col-md-4 order-2 order-md-1 text-md-left text-center mt-4 mt-md-0">
                                <h3 class="mb-3 fw-normal h3"><?php the_sub_field('heading');?></h3>
                            
                                
                                <a href="<?php the_sub_field('page_link');?>" class="button button-border py-1 nott ls0 fw-normal button-dark border-width-1 border-color h-bg-color">Shop Now</a>
                                </div>
                            <div class="col-lg-9 col-md-8 order-1 order-md-2">
                                <div class="owl-carousel frame-type-carousel ">
    
                                   <?php
                                 $cat = get_sub_field('product_taxonomy');
                                 // var_dump($cat);
                                 $cat_name = $cat[0]->name;
                                 // echo ($cat_name);

                        $products = get_posts( array(
                                                'post_type' => 'product',
                                                'numberposts' => -1,
                                                'post_status' => 'publish',
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'product_cat',
                                                        'field' => 'slug',
                                                        'terms' => $cat_name, /*category name*/
                                                        'operator' => 'IN',
                                                        )
                                                    ),
                                                ));

//                                      var_dump($products);
                                        foreach ($products as $product){
//                                          var_dump($product);
                                         get_single_product_type_html($product);
                                        
                                
                                        }
                                        ?>
									
								
            
                                </div>
                            </div>
                        </div>
                        <?php if($i == 1){?>    <hr class="my-5"> <?php }?>
					

                        <?php

                        $i++;

                        // wp_reset_postdata();
                            endwhile;
                            ?>


					
					</div>
				</div>