 <!-- 5. Section Fetured Boxes
                ============================================= -->
                <div class="section custom-bg mt-lg-0 mb-0 pt-6" style="--custom-bg: #fdf3e7;">
                    <div class="container py-lg-4">
                        <div class="row col-mb-50">

                            <?php 
                             while (have_rows('services_block')):
                                the_row();
                                ?>
                            <!-- feature-box 1 -->
                            <div class="col-md-4">
                                <div class="feature-box fbox-center fbox-dark fbox-plain">
                                    <div class="fbox-icon">
                                        <?php the_sub_field('icon'); ?>
                                    </div>
                                    <div class="fbox-content">
                                        <h2 class="nott fw-medium h4 mb-4"><?php the_sub_field('heading'); ?></h2>
                                        <p class="op-06"><?php the_sub_field('paragraph'); ?></p>
                                    </div>
                                </div>
                            </div>

                           <?php
                            endwhile;
                            ?>


                        </div>
                    </div>
                </div>
                <!-- Section End -->`