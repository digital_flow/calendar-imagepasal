<!-- 2. Section Shop by Category
                ============================================= -->
                <div class="section mb-0">
                    <div class="container-fluid">


                    <!-- Heading Title -->
                    <div class="text-center mb-5">
                        <h2 class="h1 fw-normal mb-4"><?php the_sub_field('heading'); ?>
                        </h2>
                        <a href="<?php echo get_home_url(); ?>/all-category" class="button button-small button-border m-0 button-dark border-width-1 border-default px-4 h-bg-primary"><i class="icon-line-grid"></i> <?php the_sub_field('button_text'); ?></a>
                    </div>

<div class="row item-categories gutter-20">
<?php

 $category_id = get_sub_field('pick_category');
//              var_dump($category_id);
                foreach($category_id as $cat):
                $category = get_term_by('id', $cat, 'product_cat', 'ARRAY_A');
             // var_dump($category);
                // get the thumbnail id using the queried category term_id
    $thumbnail_id = get_term_meta( $cat, 'thumbnail_id', true ); 

    // get the image URL
    $image = wp_get_attachment_url( $thumbnail_id ); 
                ?>

                   <!-- Categories -->
                    
                        <div class="col-lg-4 col-md-6">
                            <a href="<?= get_category_link($cat) ?>" class="d-block h-op-09 op-ts" style="background: url('<?php echo $image; ?>') no-repeat center center; background-size: cover; height: 340px;">
                                <h5 class="text-uppercase ls1 bg-white mb-0"><?php echo $category['name'] ?></h5>
                            </a>
                        </div>
                        
                        
                  


            <?php
    endforeach;
            ?>
      </div>


                </div>
                <!-- Section End -->    </div>