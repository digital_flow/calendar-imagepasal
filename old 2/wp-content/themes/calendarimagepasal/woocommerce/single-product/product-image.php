<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
    return;
}

global $product;

$columns = apply_filters('woocommerce_product_thumbnails_columns', 4);
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes = apply_filters('woocommerce_single_product_image_gallery_classes', array(
    'woocommerce-product-gallery',
    'woocommerce-product-gallery--' . ($product->get_image_id() ? 'with-images' : 'without-images'),
    'woocommerce-product-gallery--columns-' . absint($columns),
    'images',
));

$image_ids = $product->get_gallery_image_ids();

if (empty($image_ids)) {
    $image_ids = [$product->get_image_id()];

} else {
    array_unshift($image_ids, $product->get_image_id());
}
?>
<div class="all mb-3 mb-sm-0">
    <div class="slider magnific-gallery">
        <?php woocommerce_show_product_sale_flash() ?>
        <div class="owl-carousel owl-theme main">
            <?php
            foreach ($image_ids as $image_id):
                $image = wp_get_attachment_image_url($image_id, 'full');
                $image = getResizedImage($image);
                ?>
                <div class="item-box">
                    <a href="<?= wp_get_attachment_image_url($image_id, 'full') ?>" data-effect="mfp-zoom-in">
                        <img src="<?= wp_get_attachment_image_url($image_id, 'full') ?>" alt="" class="img-fluid"
                            data-zoomed="<?= wp_get_attachment_image_url($image_id, 'full') ?>"
                            data-image-id="<?= $image_id ?>">
                    </a>
                    
                </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
    <?php
    if (sizeof($image_ids) > 1):
        ?>
        <div class="slider-two">
            <div class="owl-carousel owl-theme thumbs ">
                <?php
                foreach ($image_ids as $image_id):
                    $image = wp_get_attachment_image_url($image_id, 'full');
                    $image = getResizedImage($image);
                    ?>
                    <div class="item">
                        
                            <img src="<?= wp_get_attachment_image_url($image_id, 'full') ?>" alt="" class="img-fluid"
                             data-thumb-image-id="<?= $image_id ?>">
                        
                    </div>
                <?php
                endforeach;
                ?>
            </div>
            <div class="left-t nonl-t"></div>
            <div class="right-t"></div>
        </div>
    <?php
    endif;
    ?>
</div>
