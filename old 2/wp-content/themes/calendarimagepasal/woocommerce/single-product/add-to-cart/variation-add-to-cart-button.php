<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $product;
?>
<div class="prod_options">
    <?php do_action('woocommerce_before_add_to_cart_button'); ?>

    <?php
    do_action('woocommerce_before_add_to_cart_quantity');

    ?>
    <div class="row">
        <label class="col-xl-2 col-lg-5  col-md-6 col-6"><strong>Quantity</strong></label>
        <div class="col-xl-3 col-lg-5 col-md-6 col-6">
            <div class="numbers-row">
                <input id="quantity_1" type="text" min="<?= $product->get_min_purchase_quantity() ?>"
                       max="<?= ($product->get_max_purchase_quantity() != -1) ? $product->get_max_purchase_quantity() : '' ?>"
                       name="quantity_1" value="1"
                       class="qty2">
            </div>
        </div>
        <?php

        do_action('woocommerce_after_add_to_cart_quantity');
        ?>
        <div class="col-lg-4 col-md-6">
            <div class="btn_add_to_cart">
                <a class="btn_1 single_add_to_cart_button"
                   value="<?php echo esc_attr($product->get_id()); ?>"><?php echo esc_html($product->single_add_to_cart_text()); ?></a>
            </div>
        </div>
        <?php do_action('woocommerce_after_add_to_cart_button'); ?>

        <input type="hidden" name="add-to-cart" value="<?php echo absint($product->get_id()); ?>"/>
        <input type="hidden" name="product_id" value="<?php echo absint($product->get_id()); ?>"/>
        <input type="hidden" name="variation_id" class="variation_id" value="0"/>
    </div>
</div>
