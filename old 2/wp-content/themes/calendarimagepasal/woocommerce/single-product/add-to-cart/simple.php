<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $product;

if (!$product->is_purchasable()) {
    return;
}

echo wc_get_stock_html($product); // WPCS: XSS ok.

if ($product->is_in_stock()) : ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>

    <div class="prod_options">
        <?php do_action('woocommerce_before_add_to_cart_button'); ?>

        <?php
        do_action('woocommerce_before_add_to_cart_quantity');

        ?>
        <div class="row">
            <label class="col-xl-2 col-lg-3 col-md-12 col-12"><strong>Quantity</strong></label>
            <div class="col-xl-3 col-lg-4 col-md-5 col-5">
                <div class="numbers-row">
                    <input type="text" value="1" id="quantity_1" class="qty2" name="quantity_1"
                           min="<?= $product->get_min_purchase_quantity() ?>"
                           max="<?= ($product->get_max_purchase_quantity() != -1) ? $product->get_max_purchase_quantity() : '' ?>">
                </div>
            </div>
            <?php
            do_action('woocommerce_after_add_to_cart_quantity');
            ?>
            <div class="col-xl-4 col-lg-5 col-md-6 col-7">
                <div class="btn_add_to_cart">
                    <a class="btn_1 single_add_to_cart_button"
                       value="<?php echo esc_attr($product->get_id()); ?>"><?php echo esc_html($product->single_add_to_cart_text()); ?></a>
                </div>
            </div>
        </div>

        <?php do_action('woocommerce_after_add_to_cart_button'); ?>
    </div>

    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>