<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.

    return;
}
?>


<!-- Content
		============================================= -->
		<section id="content product-<?php the_ID();?>">
			<div class="content-wrap py-0 overflow-visible">

				<div class="section overflow-visible mt-0">
					<div class="container">

						<!-- 1. Section
						============================================= -->
						<div class="single-product py-lg-3">

							<!-- Product
							============================================= -->
							<div class="product">

								<div class="row">

									<!-- Product Image
									============================================= -->
									<div class="col-md-6">
										<div class="row align-items-start gutter-30" data-lightbox="gallery"> <!-- .align-items-center for mobile Bugs fixed -->
											
											
											                                                        					<?php 
											
											$image_ids = $product->get_gallery_image_ids();

												if ( empty( $image_ids ) ) {
													$image_ids = [ $product->get_image_id() ];

												} else {
													array_unshift( $image_ids, $product->get_image_id() );
												}
											
													foreach ( $image_ids as $image_id ):
														$image = wp_get_attachment_image_url( $image_id, 'full');
// 											echo $images;
// 														$image = getResizedImage( $image );
														?>

											
											
											<a href="<?= wp_get_attachment_image_url( $image_id,'full') ?>" data-lightbox="gallery-item" class="col-sm-12 col-6"><img src="<?= wp_get_attachment_image_url( $image_id,'full') ?>" ></a>
											<?php
			endforeach;
			?>
											
										</div>
									</div>

									<!-- Product Description
									============================================= -->
									<div class="col-md-6 product-desc p-lg-5 px-4 py-0 mt-5 mt-md-0 content-sticky">

										<!-- Product Title
										============================================= -->
										<h2 class="mb-3 fw-normal"><?php the_title( ); ?></h2>

										
										
										<div class="row align-items-center justify-content-between">
											<div class="col-md-5">
												<!-- Product Price
										============================================= -->
										<h3 class="h2 mb-5 fw-medium"><?php echo $product->get_price_html(); ?></h3>
											</div>
											<div class="col-lg-7 text-right">
<!-- 												<button type="submit" class="single_add_to_cart_button add-to-cart button button-dark  fw-medium  px-lg-4 add-to-cart m-0">
                                            	<i class="icon-line-shopping-cart"></i>
                                            	 <a class="btn_1 single_add_to_cart_button"
                       					value="<?php echo esc_attr($product->get_id()); ?>">Inquiry Now <i class="icon-long-arrow-right"></i></a></button> -->
												
												<a id="rr" href="<?php echo esc_url( $product->add_to_cart_url() ); ?>" class="btn btn-dark add-cart btn-add-cart" title="Add to Cart"  data-product_id="<?= $product->get_id() ?>">Inquiry Now <i class="icon-long-arrow-right"></i></a>
												
											</div>
										</div>

										<!-- Product Single - Quantity & Cart Button
										============================================= -->
<!-- 										<form class="cart mb-0 d-flex align-items-center d-none" method="post" enctype='multipart/form-data'>
											<div class="quantity position-relative mb-3 clearfix">
												<input type="button" value="-" class="minus">
												<input type="number" step="1" min="1" max="3" name="quantity" value="1" title="Qty" class="qty" />
												<input type="button" value="+" class="plus">
												<div class="clear"></div>
											</div>
											<button type="submit" class="add-to-cart button button-large fw-medium button-dark  px-lg-4 add-to-cart m-0 mb-3"> Add to cart</button>
										</form> -->
										<!-- Product Single - Quantity & Cart Button End -->


										<!-- Line
										============================================= -->
										<div class="line my-5"></div>

										<!-- Product Toggle - Overview
										============================================= -->
										<div class="pb-4 qv-toogle">
											<a href="#" class="font-primary d-flex align-items-center mb-0  text-dark" data-bs-toggle="collapse" data-bs-target="#overview" aria-expanded="true" aria-controls="overview">Overview
												<i class="icon-line-plus ms-auto"></i>
												<i class="icon-line-minus ms-auto"></i>
											</a>
											<div class="collapse show" id="overview">
												<p class="op-06 fw-normal my-2"><?= the_content();	?></p>
											</div>
										</div>

										<!-- Product Toggle - Materials
										============================================= -->
										<div class="pb-4 qv-toogle">
											<a href="#" class="font-primary d-flex align-items-center mb-0  text-dark collapsed " data-bs-toggle="collapse" data-bs-target="#materials" aria-expanded="false" aria-controls="materials">Materials
												<i class="icon-line-plus ms-auto"></i>
												<i class="icon-line-minus ms-auto"></i>
											</a>
											<div class="collapse" id="materials">
												<p class="op-06 fw-normal my-2"><?= the_excerpt();	?></p>
											</div>
										</div>

										<!-- Product Toggle - Installation & Warrenty
										============================================= -->
										<div class="pb-4 qv-toogle">
											<a href="#" class="font-primary d-flex align-items-center mb-0  text-dark collapsed " data-bs-toggle="collapse" data-bs-target="#installation" aria-expanded="false" aria-controls="installation">Installation &amp; Warrenty
												<i class="icon-line-plus ms-auto"></i>
												<i class="icon-line-minus ms-auto"></i>
											</a>
											<div class="collapse" id="installation">
												<p class="op-06 fw-normal my-2">Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.</p>
											</div>
										</div>

										<!-- Product Toggle - Returns & Cancellations
										============================================= -->
										<div class="pb-4 qv-toogle">
											<a href="#" class="font-primary d-flex align-items-center mb-0  text-dark collapsed " data-bs-toggle="collapse" data-bs-target="#return" aria-expanded="false" aria-controls="return">Returns &amp; Cancellations
												<i class="icon-line-plus ms-auto"></i>
												<i class="icon-line-minus ms-auto"></i>
											</a>
											<div class="collapse" id="return">
												<p class="op-06 fw-normal my-2">Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.</p>
											</div>
										</div>

									</div>
								</div>

							</div>

						</div><!-- Section End -->

					</div>

				</div>

				
								<?php if( ! is_a( $product, 'WC_Product' ) ){
    $product = wc_get_product(get_the_id());
}

woocommerce_related_products( array(
    'posts_per_page' => 4,
    'columns'        => 4,
    'orderby'        => 'rand'
) ); ?>
				
				

				<!-- 5. Section Fetured Boxes
				============================================= -->
				<div class="section custom-bg mb-0 py-6" style="--custom-bg: #fdf3e7;">
					<div class="container py-lg-4">
						<div class="row col-mb-50">

							<!-- feature-box 1 -->
							<div class="col-md-4">
								<div class="feature-box fbox-center fbox-dark fbox-plain">
									<div class="fbox-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><path d="M54.46089,201.53911c-9.204-9.204-3.09935-28.52745-7.78412-39.85C41.82037,149.95168,24,140.50492,24,127.99963,24,115.4945,41.82047,106.048,46.67683,94.31079c4.68477-11.32253-1.41993-30.6459,7.78406-39.8499s28.52746-3.09935,39.85-7.78412C106.04832,41.82037,115.49508,24,128.00037,24c12.50513,0,21.95163,17.82047,33.68884,22.67683,11.32253,4.68477,30.6459-1.41993,39.8499,7.78406s3.09935,28.52746,7.78412,39.85C214.17963,106.04832,232,115.49508,232,128.00037c0,12.50513-17.82047,21.95163-22.67683,33.68884-4.68477,11.32253,1.41993,30.6459-7.78406,39.8499s-28.52745,3.09935-39.85,7.78412C149.95168,214.17963,140.50492,232,127.99963,232c-12.50513,0-21.95163-17.82047-33.68884-22.67683C82.98826,204.6384,63.66489,210.7431,54.46089,201.53911Z" opacity="0.2"></path><path d="M54.46089,201.53911c-9.204-9.204-3.09935-28.52745-7.78412-39.85C41.82037,149.95168,24,140.50492,24,127.99963,24,115.4945,41.82047,106.048,46.67683,94.31079c4.68477-11.32253-1.41993-30.6459,7.78406-39.8499s28.52746-3.09935,39.85-7.78412C106.04832,41.82037,115.49508,24,128.00037,24c12.50513,0,21.95163,17.82047,33.68884,22.67683,11.32253,4.68477,30.6459-1.41993,39.8499,7.78406s3.09935,28.52746,7.78412,39.85C214.17963,106.04832,232,115.49508,232,128.00037c0,12.50513-17.82047,21.95163-22.67683,33.68884-4.68477,11.32253,1.41993,30.6459-7.78406,39.8499s-28.52745,3.09935-39.85,7.78412C149.95168,214.17963,140.50492,232,127.99963,232c-12.50513,0-21.95163-17.82047-33.68884-22.67683C82.98826,204.6384,63.66489,210.7431,54.46089,201.53911Z" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></path><polyline points="172 104 113.333 160 84 132" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></polyline></svg>
									</div>
									<div class="fbox-content">
										<h2 class="nott fw-medium h4 mb-4">Original Quality</h2>
										<p class="op-06">Completely formulate top-line resources rather than cross-media portals cross-platform solutions.</p>
									</div>
								</div>
							</div>

							<!-- feature-box 2 -->
							<div class="col-md-4">
								<div class="feature-box fbox-center fbox-dark fbox-plain">
									<div class="fbox-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><path d="M176,144H16v40a8,8,0,0,0,8,8H44a24,24,0,0,1,48,0h72a23.99048,23.99048,0,0,1,11.99813-20.78815Z" opacity="0.2"></path><path d="M212,192a24.00631,24.00631,0,0,0-36.00187-20.78815L176,120h64v64a8,8,0,0,1-8,8H212" opacity="0.2"></path><path d="M240,120H176V80h42.58374a8,8,0,0,1,7.42781,5.02887Z" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></path><line x1="16" y1="144" x2="176" y2="144" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></line><circle cx="188" cy="192" r="24" fill="none" stroke="var(--themecolor)" stroke-miterlimit="10" stroke-width="8"></circle><circle cx="68" cy="192" r="24" fill="none" stroke="var(--themecolor)" stroke-miterlimit="10" stroke-width="8"></circle><line x1="164" y1="192" x2="92" y2="192" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></line><path d="M44,192H24a8,8,0,0,1-8-8V72a8,8,0,0,1,8-8H176V171.21508" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></path><path d="M176,171.21508V120h64v64a8,8,0,0,1-8,8H212" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></path></svg>
									</div>
									<div class="fbox-content">
										<h2 class="nott fw-medium h4 mb-4">Free & Fast Shipping</h2>
										<p class="op-06">Rapidiously optimize user-centric catalysts for change vis-a-vis granular "outside the box" thinking.</p>
									</div>
								</div>
							</div>

							<!-- feature-box 3 -->
							<div class="col-md-4">
								<div class="feature-box fbox-center fbox-dark fbox-plain">
									<div class="fbox-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="var(--themecolor)" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><path d="M40,106.66667V48a8,8,0,0,1,8-8H208a8,8,0,0,1,8,8v58.66667c0,84.01533-71.306,111.85016-85.5438,116.57058a7.54755,7.54755,0,0,1-4.9124,0C111.306,218.51683,40,190.682,40,106.66667Z" opacity="0.2"></path><path d="M40,106.66667V48a8,8,0,0,1,8-8H208a8,8,0,0,1,8,8v58.66667c0,84.01533-71.306,111.85016-85.5438,116.57058a7.54755,7.54755,0,0,1-4.9124,0C111.306,218.51683,40,190.682,40,106.66667Z" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></path><polyline points="172 96 113.333 152 84 124" fill="none" stroke="var(--themecolor)" stroke-linecap="round" stroke-linejoin="round" stroke-width="8"></polyline></svg>
									</div>
									<div class="fbox-content">
										<h2 class="nott fw-medium h4 mb-4">Secure Payment</h2>
										<p class="op-06">Continually recaptiualize 2.0 action items after global information. Efficiently strategize holistic networks.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Section End -->

			</div>
		</section><!-- #content end -->
											
											<?php do_action('woocommerce_after_single_product'); ?>
