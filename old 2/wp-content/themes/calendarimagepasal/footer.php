<footer id="footer" class="border-top bg-white">
            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap py-lg-6">
                    <div class="row  justify-content-between">
                        <div class="col-md-6 col-lg-3 align-self-md-center pb-0">
                            <div class="widget widget_ftr-logo text-md-center">
                                <h4 class="mb-0 justify-content-md-center">product of </h4>
                                <a href="https://nirvanstudio.com/" target="_blank"><img src="https://nirvanstudio.com/wp-content/uploads/2021/05/Nirvan-Studio-04.svg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-4 border-left pl-lg-5">
                            <div class="widget widget_links widget-li-noicon widget_categories">
                                <h4 class="ls0 nott">Popular Category <i class="icon-angle-down"></i></h4>
                                <div class="widget_slide-content">
                                    <ul class="list-unstyled iconlist ms-0">
                                        <?php 
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'popular',
                                    'items_wrap' => '<li> %3$s</li> '
                                )
                            );
                        ?>
                                    </ul>
                                </div>
                                

                            </div>
                        </div>
                        <div class="col-lg-2 ">
                            <div class="widget widget_links widget-li-noicon">
                                <h4 class="ls0 nott">Quick Links <i class="icon-angle-down"></i></h4>
                                <div class="widget_slide-content">
                                    <ul class="list-unstyled iconlist ms-0">
                                                 <?php 
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'footer',
//                                      'add_li_class'  => 'menu-item',
//                                      'add_a_class'     => 'menu-link',
                                    'items_wrap' => '<li> %3$s</li> '
                                )
                            );
                        ?>
                                       </ul>
                                </div>
                                

                            </div>
                        </div>
                        <div class="col-lg-2 ">
                            <div class="widget widget_links widget-li-noicon widget_links__social">

                                <h4 class="ls0 nott">Follow Us <i class="icon-angle-down"></i></h4>
                                <div class="widget_slide-content">
                                    <ul class="list-unstyled iconlist ms-0">
                                         <?php $social = get_field('social', 'option'); ?>
                                    
                                    <?php if($social['facebook']) { ?>
                                    <li><a href="<?=  $social['facebook']; ?>" target="_blank"><i class="icon-facebook"></i> </a></li>
                                    <?php } ?>
                                    <?php if($social['instagram']) { ?>
                                    <li><a href="<?= $social['instagram']; ?>" target="_blank"><i class="icon-instagram"></i> </a></li>
                                    <?php } ?>
                                    <?php if($social['twitter']) { ?>
                                    <li><a href="<?= $social['twitter']; ?>" target="_blank"><i class="icon-twitter"></i> </a></li>
                                    <?php } ?>
                                    </ul>
                                </div>
                                

                            </div>
                            
                        </div>
                    </div>

                </div>
                <!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights" class="py-3 bg-color-light">
                <div class="container">

                    <div class="d-flex justify-content-center op-04">
                        <span>© 2021 All Rights Reserved by IMAGEPASAL.</span>
                        <!-- <div class="copyright-links"><a href="#">Termsasdasdasd of Use</a> / <a href="#">Privacy Policy</a></div> -->
                    </div>

                </div>
            </div>
            <!-- #copyrights end -->
        </footer>


<!-- Footer
        ============================================= -->

<footer id="footer" class="border-0 bg-white d-none">
            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap py-lg-6">
                    <div class="row col-mb-30 justify-content-between">
                        <div class="col-lg-2 align-self-md-center">
                            <div class="widget text-center">
                                <h4 class="mb-0">product of </h4>
                                <a href="https://www.nirvanstudio.com" target="_blank"><img src="https://nirvanstudio.com/wp-content/uploads/2021/05/Nirvan-Studio-04.svg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 border-left pl-md-5">
                            <div class="widget widget_links ">
                                <h4 class="ls0 nott">Catgories</h4>
                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="https://print.imagepasal.com/product-category/products/canvas-print/">Canvas</a></li>
                                    <li><a href="https://print.imagepasal.com/product-category/products/framed-print/">Frame</a></li>
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Get to know us</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <?php 
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'footer',
                                    'container' => '',
//                                      'add_li_class'  => 'menu-item',
//                                      'add_a_class'     => 'menu-link',
                                    'items_wrap' => '<li> %3$s</li> '
                                )
                            );
                        ?>

                                    
<!--                                     <li><a href="demo-furniture.html">Home</a></li>
                                    <li><a href="demo-furniture-about.html">About</a></li>
                                    <li><a href="demo-furniture-contact.html">Contact</a></li> -->
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4">
                            <div class="widget widget_links widget-li-noicon widget_links__social">

                                <h4 class="ls0 nott">Social</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                     <?php $social = get_field('social', 'option'); ?>
                                    
                                    <?php if($social['facebook']) { ?>
                                    <li><a href="<?=  $social['facebook']; ?>" target="_blank"><i class="icon-facebook"></i> </a></li>
                                    <?php } ?>
                                    <?php if($social['instagram']) { ?>
                                    <li><a href="<?= $social['instagram']; ?>" target="_blank"><i class="icon-instagram"></i> </a></li>
                                    <?php } ?>
                                    <?php if($social['twitter']) { ?>
                                    <li><a href="<?= $social['twitter']; ?>" target="_blank"><i class="icon-twitter"></i> </a></li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="widget subscribe-widget clearfix" data-loader="button">
                                <h4>Subscribe Us</h4>
                                <h5 class="font-body op-04"><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
                                <div class="widget-subscribe-form-result"></div>
                                <form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" method="post" class="mb-0">
                                    <div class="input-group">
                                        <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email Address">
                                        <button class="button button-primary px-3 input-group-text m-0" type="submit">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row col-mb-30 d-none">

                        <!-- Footer Col 1 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott"></h4>

                                <ul class="list-unstyled iconlist ms-0">
                                     
                                    
                                    <?php if($social['facebook']) { ?>
                                    <li><a href="<?=  $social['facebook']; ?>" target="_blank"><i class="icon-facebook"></i> </a></li>
                                    <?php } ?>
                                    <?php if($social['instagram']) { ?>
                                    <li><a href="<?= $social['instagram']; ?>" target="_blank"><i class="icon-instagram"></i> </a></li>
                                    <?php } ?>
                                    <?php if($social['twitter']) { ?>
                                    <li><a href="<?= $social['twitter']; ?>" target="_blank"><i class="icon-twitter"></i> </a></li>
                                    <?php } ?>

                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 2 -->
                        <div class="col-lg-2 col-md-3 col-6">
                             <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Quick Links:</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <?php 
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'footer',
                                    'container' => '',
//                                      'add_li_class'  => 'menu-item',
//                                      'add_a_class'     => 'menu-link',
                                    'items_wrap' => '<li> %3$s</li> '
                                )
                            );
                        ?>

                                    
<!--                                     <li><a href="demo-furniture.html">Home</a></li>
                                    <li><a href="demo-furniture-about.html">About</a></li>
                                    <li><a href="demo-furniture-contact.html">Contact</a></li> -->
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 3 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Trending</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="demo-furniture-products.html">Shop</a></li>
                                    <li><a href="demo-forum-single.html">Single</a></li>
                                    <li><a href="demo-furniture-about.html">Who are we</a></li>
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 4 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Get to Know us</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="intro.html#section-niche">Niche Demos</a></li>
                                    <li><a href="intro.html#section-multipage">Home Pages</a></li>
                                    <li><a href="intro.html#section-onepage">One Pages</a></li>
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 5 -->
                        <div class="col-lg-4">
                            <div class="widget subscribe-widget clearfix" data-loader="button">
                                <h4>Subscribe Us</h4>
                                <h5 class="font-body op-04"><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
                                <div class="widget-subscribe-form-result"></div>
                                <form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" method="post" class="mb-0">
                                    <div class="input-group">
                                        <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email Address">
                                        <button class="button button-primary px-3 input-group-text m-0" type="submit">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights" class="py-3 bg-color-light">
                <div class="container">

                    <div class="d-flex justify-content-center op-04">
                        <span>&copy; <?php echo date('Y') ?> All Rights Reserved by ImagePasal</span>
                        <div class="copyright-links d-none"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                    </div>

                </div>
            </div>
            <!-- #copyrights end -->
        </footer>





        <footer id="footer" class="border-width-1 border-default bg-white d-none">
            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap py-lg-6">
                    <div class="row col-mb-30">

                        <!-- Footer Col 1 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Social</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <?php if($social['facebook']) { ?>
                                    <li><a href="<?=  $social['facebook']; ?>" target="_blank"><i class="icon-facebook"></i> Facebook</a></li>
                                    <?php } ?>
                                    <?php if($social['instagram']) { ?>
                                    <li><a href="<?= $social['instagram']; ?>" target="_blank"><i class="icon-instagram"></i> Instagram</a></li>
                                    <?php } ?>
                                    <?php if($social['twitter']) { ?>
                                    <li><a href="<?= $social['twitter']; ?>" target="_blank"><i class="icon-twitter"></i> Twitter</a></li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 2 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Quick Links:</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <?php 
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'footer',
                                    'container' => '',
//                                      'add_li_class'  => 'menu-item',
//                                      'add_a_class'     => 'menu-link',
                                    'items_wrap' => '<li> %3$s</li> '
                                )
                            );
                        ?>

                                    
<!--                                     <li><a href="demo-furniture.html">Home</a></li>
                                    <li><a href="demo-furniture-about.html">About</a></li>
                                    <li><a href="demo-furniture-contact.html">Contact</a></li> -->
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 3 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Trending</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="demo-furniture-products.html">Shop</a></li>
                                    <li><a href="demo-forum-single.html">Single</a></li>
                                    <li><a href="demo-furniture-about.html">Who are we</a></li>
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 4 -->
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="widget widget_links widget-li-noicon">

                                <h4 class="ls0 nott">Get to Know us</h4>

                                <ul class="list-unstyled iconlist ms-0">
                                    <li><a href="intro.html#section-niche">Niche Demos</a></li>
                                    <li><a href="intro.html#section-multipage">Home Pages</a></li>
                                    <li><a href="intro.html#section-onepage">One Pages</a></li>
                                </ul>

                            </div>
                        </div>

                        <!-- Footer Col 5 -->
                        <div class="col-lg-4">
                            <div class="widget subscribe-widget clearfix" data-loader="button">
                                <h4>Subscribe Us</h4>
                                <h5 class="font-body op-04"><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
                                <div class="widget-subscribe-form-result"></div>
                                <form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" method="post" class="mb-0">
                                    <div class="input-group">
                                        <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email Address">
                                        <button class="button button-primary px-3 input-group-text m-0" type="submit">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights" class="py-3 bg-color-light">
                <div class="container">

                    <div class="d-flex justify-content-between op-04">
                        <span>&copy; <?php echo date('Y') ?> All Rights Reserved by ImagePasal</span>
                        <!-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> -->
                    </div>

                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->

    </div>
    <!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="bg-color op-07 h-op-1">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#FFF" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><polygon points="48 208 128 128 208 208 48 208" opacity="0.2"></polygon><polygon points="48 208 128 128 208 208 48 208" fill="none" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polygon><polyline points="48 128 128 48 208 128" fill="none" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline></svg>
    </div>







<?php wp_footer(); ?>
<script>
    
        jQuery(document).ready( function($){

            var lastScrollTop = 0;
            window.addEventListener("scroll", function(event){
                var st = $(this).scrollTop();
                if (st > lastScrollTop){
                    jQuery('#header.sticky-on-scrollup').removeClass('show-sticky-onscroll'); // Down Scroll
                } else {
                    jQuery('#header.sticky-on-scrollup').addClass('show-sticky-onscroll'); // Up Scroll
                }
                lastScrollTop = st;
            });

        });
    
</script>
    

</body>

</html>