<?php
define( 'WP_CACHE', true ) ;

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'calendar');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '0r4v~g@Cefi~Z]<Z@Vo/b-j,B2S@Dh}#!Y!d9}%IzD?L>VvuNSGPLA,yfQBK&Wa%');
define('SECURE_AUTH_KEY', 'X1VVP <*sN~E|A.V*E44&w~T=q5FDj)@]PC=(%Ui`a1!5eWd[</oC^SHA@@]-WeP');
define('LOGGED_IN_KEY', '#:yu))pxpBa@Ap7,+QpY86KPTlrQ(eGK))m5~T2rsfw?Mum`T0-eadtpV)MICa=8');
define('NONCE_KEY', ';c:CuLjYSQyCstB1[v;6>OX[K[+6_{OS&Kva,s(dzjj}=^3JV;Z+F1u2&wnNA4 m');
define('AUTH_SALT', '~D?W{ 8lm`_`s8G,;B[.os6[0o;+T+!UNrUoU/{A/+;6~8fpTn_Ld@S[MOF#07]F');
define('SECURE_AUTH_SALT', '*tu^!w}4Khq-brA@9Y~IU0T5+KSvyaF3F, Yz0Z4|og!oP69k/s5@$A: 7!QZDE$');
define('LOGGED_IN_SALT', 'z&)QSxnZTi_.8M|p~K]4,&[:ntudbaM.rFek@SdCTr2@r6[L:_hsXPKGog@_TN)3');
define('NONCE_SALT', 'UOd?K,d-ppfbIrI)q/<)@]R->:kH^6ZPwl)/DuOiBi.VC| .JZ5Q}WeR.DfMde`^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ag_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', true);
define('WP_POST_REVISIONS', 3);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
