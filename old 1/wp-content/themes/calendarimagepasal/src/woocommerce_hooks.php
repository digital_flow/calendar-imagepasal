<?php

// Remove meta hook Woocommerce
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// Remove product sharing hook Woocommerce
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


/**
 * Change number of related products output
 */
add_filter('woocommerce_output_related_products_args', 'ecommerce_related_products_args', 20);
function ecommerce_related_products_args($args)
{

    $args['posts_per_page'] = 10; // 4 related products

    return $args;
}

/**
 * Change number of upsells output
 */
add_filter('woocommerce_upsell_display_args', 'wc_change_number_related_products', 20);

function wc_change_number_related_products($args)
{

    $args['posts_per_page'] = 10;

    return $args;
}


/**
 * integrate product sharing in product single page
 */
add_action('woocommerce_share', 'aatti_single_product_sharing');
function aatti_single_product_sharing()
{
    global $product;

    $facebook_url = "https://www.facebook.com/sharer.php?u=" . $product->get_permalink();
    $twitter_url = add_query_arg(
        [
            'text' => urlencode($product->get_title()),
            'url' => $product->get_permalink(),
            'hashtags' => 'revivalpoint'
        ],
        "https://www.twitter.com/intent/tweet?"
    );

    $mail_body = $product->get_short_description() . " For details, link here : " . $product->get_permalink();

    $gmail_url = add_query_arg(
        [
            'view' => 'cm',
            'fs' => 1,
            'to' => '',
            'su' => urlencode($product->get_title()),
            'body' => urlencode($mail_body),
            'bcc' => ''
        ],
        "https://mail.google.com/mail/"
    );
    ?>
    <div class="product_share">
        <ul>
            <li>
                <a href="<?= $facebook_url ?>" target="_blank" rel="noreferrer noopener"><i
                            class="fab fa-facebook-f"></i></a>
            </li>
            <li>
                <a href="<?= $twitter_url ?>" target="_blank" rel="noreferrer noopener"><i
                            class="fab fa-twitter"></i></a>
            </li>
            <li>
                <a href="<?= $gmail_url ?>" target="_blank" rel="noreferrer noopener"><i class="fab fa-google"></i></a>
            </li>
        </ul>
    </div>
    <?php
}

/**
 * adding additional delivery info tab to the product single page tabsg
 */
//add_filter('woocommerce_product_tabs', 'woo_custom_product_tabs');
function woo_custom_product_tabs($tabs)
{
    if (get_field('delivery_info', 'option')['detail']) {
        $tabs['additional_tab'] = [
            'title' => get_field('delivery_info', 'option')['title'],
            'priority' => 100,
            'callback' => 'woo_attrib_additional_tab_content'
        ];
    }

    return $tabs;
}

function woo_attrib_additional_tab_content()
{
    echo get_field('delivery_info', 'option')['detail'];
}


/**
 *  add order_by_phone button after add_to_cart button
 */
function happyshoppe_order_by_phone_button()
{

    $contact = get_field('contact', 'option');
    ?>
    <a href="tel:<?= $contact['phone_number'] ?>" class="button text-uppercase pull-left btn ml-3">order by
        phone</a>
    <?php
}

//add_action('woocommerce_after_add_to_cart_button', 'happyshoppe_order_by_phone_button');


/**
 *  override default function to show product title in product loop
 */
function woocommerce_template_loop_product_title()
{
    echo '<h3 class="' . esc_attr(apply_filters('woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title')) . '">' . get_the_title() . '</h3>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

add_action('woocommerce_product_query', 'aatti_product_query');

function aatti_product_query($q)
{
    if (isset($_GET['onsale'])) {
        $product_ids_on_sale = wc_get_product_ids_on_sale();

        $q->set('post__in', $product_ids_on_sale);

    }

}