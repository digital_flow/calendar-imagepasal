<?php
/**
 * update default woocommerce address fields
 *
 * @param $fields
 *
 * @return mixed
 */
function aatti_default_address_fields($fields)
{
    unset($fields['last_name']);
    unset($fields['address_2']);
    unset($fields['company']);
    unset($fields['country']);
    unset($fields['address_2']);
    unset($fields['city']);
    unset($fields['state']);
    unset($fields['postcode']);

    $fields['first_name']['label'] = 'Full Name';
    $fields['first_name']['class'] = ['form-row-wide'];

    $fields['address_1']['label'] = 'Address';
    $fields['address_1']['placeholder'] = 'Address';
    $fields['address_1']['type'] = 'textarea';
    $fields['address_1']['required'] = false;

    return $fields;
}

add_filter('woocommerce_default_address_fields', 'aatti_default_address_fields', 20, 1);


function aatti_billing_fields_update($fields)
{

    foreach ($fields as $key => $field) {
        $fields[$key]['input_class'] = ['form-control'];

    }

    return $fields;
}

add_filter('woocommerce_billing_fields', 'aatti_billing_fields_update');

function aatti_shipping_fields_update($fields)
{

    foreach ($fields as $key => $field) {
        $fields[$key]['input_class'] = ['form-control'];

    }

    return $fields;
}

add_filter('woocommerce_shipping_fields', 'aatti_shipping_fields_update');

//remove additional information field from checkout page
add_filter('woocommerce_enable_order_notes_field', '__return_false', 9999);

//Remove "(optional)" from our non required fields
add_filter('woocommerce_form_field', 'remove_checkout_optional_fields_label', 10, 4);
function remove_checkout_optional_fields_label($field, $key, $args, $value)
{
// Only on checkout page
    if (is_checkout() && !is_wc_endpoint_url()) {
        $optional = '&nbsp;<span class="optional">(' . 'optional' . ')</span>';
        $field = str_replace($optional, '', $field);
    }

    return $field;
}