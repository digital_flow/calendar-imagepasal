<?php

use function NextGenImage\getImageInWebp;
use function NextGenImage\resizeImage;

/**
 * Function to get resized image in webp and original format
 *
 * @param $url string
 * @param array $size =[with x height]
 *
 * @return array
 */
function getResizedImage($url, $size = array())
{
    $webpImage = getImageInWebp(ABSPATH . str_replace(site_url(), "", $url), $size);
    $fileType = wp_check_filetype($url);
    $image = resizeImage(ABSPATH . str_replace(site_url(), "", $url), $fileType['ext'], $size);

    return array(
        'webp' => $webpImage,
        'orig' => $image
    );
}

/**
 * function to add meta tags to event single page.
 * these meta tags are required for proper functioning of facebook share feature
 */
function aatti_share_meta()
{
    global $post;

    if ($post) {
        if ($post->post_type == "product" && is_single()) {
            $product = wc_get_product($post->ID);

            $image = wp_get_attachment_image_url($product->get_image_id(), 'full');
            $image = getResizedImage($image, [500, 500]);
            ?>
            <meta property="og:url" content="<?= $product->get_permalink() ?>"/>
            <meta property="og:type" content="website"/>
            <meta property="og:title" content="<?php bloginfo('title'); ?>"/>
            <meta property="og:description" content="<?= $product->get_title() ?>"/>
            <meta property="og:image" content="<?= $image['orig'] ?>"/>
            <meta property="og:image:width" content="500"/>
            <meta property="og:image:height" content="500"/>
            <?php
        }
    }
}

add_action('wp_head', 'aatti_share_meta');


/**
 * function to get url of wishlist page
 *
 * @return false|string
 */
function aatti_get_wishlist_page_url()
{
    $wishlist_page = get_page_by_title('Wishlist');

    return get_the_permalink($wishlist_page->ID);

}

/**
 * function to get display name of logged in user
 *
 * @return string
 */
function aatti_get_user_display_name()
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $username = $user->display_name;
    }

    return $username;
}


/**
 * function to display user user account link
 */
function aatti_user_account_link()
{
    if (!is_user_logged_in()):
        ?>
        <a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')) ?>"
           class="text-default">Sign In</a> or
        <a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')) ?>"
           class="text-default">
            Register</a>
    <?php
    endif;

    if (is_user_logged_in()):
        ?>
        <a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')) ?>"
           class="text-default">User Dashboard</a>
    <?php
    endif;
}

/**
 * for product sharing in product quick view
 */
function aatti_product_sharing($product_id)
{
    $product = wc_get_product($product_id);

    $facebook_url = "https://www.facebook.com/sharer.php?u=" . $product->get_permalink();
    $twitter_url = add_query_arg(
        [
            'text' => urlencode($product->get_title()),
            'url' => $product->get_permalink(),
            'hashtags' => 'revivalpoint'
        ],
        "https://www.twitter.com/intent/tweet?"
    );

    $mail_body = $product->get_short_description() . " For details, link here : " . $product->get_permalink();

    $gmail_url = add_query_arg(
        [
            'view' => 'cm',
            'fs' => 1,
            'to' => '',
            'su' => urlencode($product->get_title()),
            'body' => urlencode($mail_body),
            'bcc' => ''
        ],
        "https://mail.google.com/mail/"
    );
    ?>
    <b>Share</b>:
    <span class="b-share_product">
                            <a href="<?= $facebook_url ?>" target="_blank" rel="noreferrer noopener"
                               class="fa fa-facebook"></a>
                            <a href="<?= $twitter_url ?>" target="_blank" rel="noreferrer noopener"
                               class="fa fa-twitter"></a>
                            <a href="<?= $gmail_url ?>" target="_blank" rel="noreferrer noopener"
                               class="fa fa-envelope"></a>
                          </span>
    <?php
}

function display_sub_categories($term, $taxonomy)
{
    $this_terms = get_terms('product_cat', ['child_of' => $term->term_id]);
    echo '<li><span><a href="' . get_category_link($term->term_id) . '">' . $term->name . '</a></span>';
    if ($this_terms) {
        echo '<ul>';
        foreach ($this_terms as $the_term) {
            if ($the_term->parent == $term->term_id) {
                display_sub_categories($the_term, $taxonomy);
            }
        }
        echo '</ul>';
    }
    echo '</li>';
}

function aatti_display_product_enquiry_buttons()
{
    $enquiry_number = get_field('enquiry_number', 'option');
    ?>
    <label>Inquiry By :</label>
    <ul class="list-unstyled">
        <?php
        if ($enquiry_number['phone']) {
            echo "<li><a href='tel:{$enquiry_number['phone']}' class='phone tooltip-1' 
                   target='_blank' rel='noreferrer noopener' title='Phone'><i class='fas fa-phone'></i></a></li>";
        }

        if ($enquiry_number['whatsapp']) {
            echo "<li><a href='https://wa.me/{$enquiry_number['whatsapp'] }' class='whatsapp tooltip-1' 
                   target='_blank' rel='noreferrer noopener' title='Whatsapp'><i class='fab fa-whatsapp'></i></a></li>";
        }

        if ($enquiry_number['phone']) {
            echo "<li><a href='viber://chat?number=%2B{$enquiry_number['viber']}' class='viber tooltip-1' target='_blank' rel='noreferrer noopener' title='Viber'><i class='fab fa-viber'></i></a></li>";
        }
        ?>
    </ul>
    <?php
}

function aatti_get_on_sale_product_ids()
{
    $args = [
        'fields' => 'ids',
        'post_type' => 'product',
        'posts_per_page' => 10,
        'meta_query' => [
            'relation' => 'OR',
            [// Simple products type
                'key' => '_sale_price',
                'value' => 0,
                'compare' => '>',
                'type' => 'numeric'
            ],
            [ // Variable products type
                'key' => '_min_variation_sale_price',
                'value' => 0,
                'compare' => '>',
                'type' => 'numeric'
            ]
        ]
    ];

    return get_posts($args);
}

function aatti_get_recent_product_ids()
{
    $args = [
        'fields' => 'ids',
        'post_type' => 'product',
        'posts_per_page' => 10,
        'orderby' => 'DATE',
        'order' => 'DESC'
    ];

    return get_posts($args);
}

function aatti_get_category_product_ids($cat_slug)
{
    $args = [
        'fields' => 'ids',
        'post_type' => 'product',
        'posts_per_page' => 10,
        'tax_query' => [
            [
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $cat_slug
            ]
        ]
    ];

    return get_posts($args);
}