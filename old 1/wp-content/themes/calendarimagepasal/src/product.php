<?php

function get_product_quick_view_html($product_id, $variation = [])
{
    $product = wc_get_product($product_id);
    $parent_product = '';
    if ($product->get_parent_id()) {
        $parent_product = wc_get_product($product->get_parent_id());
    }
    ?>
    <button type="button" class="close quick-view-modal-close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="row">
        <div class="col-md-6">
            <div class="all">
                <div class="slider overflow-hidden">
                    <div class="owl-carousel owl-theme main" id="product_popup_slider">
                        <?php

                        $image_ids = $product->get_gallery_image_ids();

                        if (empty($image_ids)) {
                            $image_ids = [$product->get_image_id()];

                        } else {
                            array_unshift($image_ids, $product->get_image_id());
                        }


                        if ($image_ids):
                            foreach ($image_ids as $image_id):
                                ?>
                                <div class="item-box">
                                    <?php
                                    $image = wp_get_attachment_image_url($image_id, 'full');
                                    $image = getResizedImage($image, [500, 500]);
                                    echo \NextGenImage\getWebPHTML($image['webp'], $image['orig'], [
                                        'alt' => esc_attr($product->get_name())
                                    ]);
                                    ?>
                                </div>
                            <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                    <div class="left nonl"><i class="ti-angle-left"></i></div>
                    <div class="right nonr"><i class="ti-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="prod_info">
                <h2><?= $product->get_name() ?></h2>
                <div class="price_main mb-3">
                    <?= $product->get_price_html() ?>
                </div>
                <p><small>SKU: <?= $product->get_sku() ?></small><br>
                    <?= $product->get_short_description() ?>
                </p>
                <div class="prod_options p-0">
                    <div class="row">
                        <label class="col-xl-12 col-lg-5  col-md-6 col-6"><strong>Quantity</strong></label>
                        <div class="col-xl-5 col-lg-5 col-md-6 col-6">
                            <div class="numbers-row">
                                <input type="text" value="1" id="quantity_1" class="qty2" name="quick_quantity"
                                       min="<?= $product->get_min_purchase_quantity() ?>"
                                       max="<?= ($product->get_max_purchase_quantity() != -1) ? $product->get_max_purchase_quantity() : '' ?>">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-4 col-md-6">
                            <div class="btn_add_to_cart">
                                <a href="<?= $product->add_to_cart_url() ?>"
                                   class="btn_1 quick-add-to-cart"
                                   data-product_id="<?= ($parent_product) ? $parent_product->get_id() : $product->get_id() ?>"
                                   data-product-type="<?= $product->get_type() ?>">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /prod_info -->
            <div class="product_inquiry border-bottom-0">
                <?php aatti_display_product_enquiry_buttons(); ?>
            </div>
            <div class="product_actions">
                <ul>
                    <li>
                        <?php echo do_shortcode("[ti_wishlists_addtowishlist product_id='" . $product->get_id() . "']") ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php
}

function get_single_product_html($product_id)
{
    $product = wc_get_product($product_id);
    ?>


  <!-- Product 1 -->
                            <div class="product col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="grid-inner">
                                    <div class="product-image">
                                        <a href="<?= $product->get_permalink();?> ">
                                                <?php $image = wp_get_attachment_image_url($product->get_image_id(), 'large'); ?>
                                                <img src="<?php echo $image; ?>" alt="" ></a>

                                                <div class="bg-overlay d-none">
                                            <div class="bg-overlay-content align-items-end justify-content-between" data-hover-animate="fadeIn" data-hover-speed="400">
                                                <a href="#" class="btn btn-light me-2"><i class="icon-line-shopping-cart"></i></a>
                                                <a href="demos/furniture/ajax/quick-view.html" class="btn btn-light" data-lightbox="ajax"><i class="icon-line-expand"></i></a>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="product-desc">
                                        <div class="product-title mb-2">
                                            <h4 class="mb-0"><a class="fw-medium" href="<?= $product->get_permalink();?> "><?= $product->get_name() ?></a></h4>

                                            <a href="<?= $product->get_permalink();?>" class="inquiry-link">Inquiry Now <i class="icon-long-arrow-right"></i></a>

                                        </div>
                 <h5 class="product-price fw-normal d-none">  
                                            <?php 
                                            $s_price = $product->get_sale_price();
                                             if (!empty($s_price)) { ?>

                                        <del class="old-price">Rs. <?php echo $product->get_regular_price(); ?></del>
                                        <span class="product-price">Rs. <?php echo $product->get_sale_price(); ?></span>      
                                           
                                          <?php  
                                          }
                                           else{ ?>
                                             <span class="product-price">Rs. <?php echo $product->get_regular_price(); ?></span> 

                                          <?php  } ?>
                                        
                                        </h5>
                                             
      
                                    </div>
                                </div>
                            </div>


 <?php
}

function get_single_product_type_html($product_id)
{
   $product = wc_get_product($product_id);
    ?>


      <div class="oc-item">
                                        <div class="product">
                                            <div class="product-image">
                                                <a href="<?php echo $product->get_permalink(); ?>" class="canvas-image">
                                                    <?php $image = wp_get_attachment_image_url($product->get_image_id(), 'large'); ?>
                                                    
                                                    <img src="<?php echo $image; ?>" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>


    <?php
}