<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_cart');

?>
<form class="woocommerce-cart-form table table-striped cart-list" action="<?php echo esc_url(wc_get_cart_url()); ?>"
      method="post">
    <?php do_action('woocommerce_before_cart_table'); ?>

    <table class="table htable-over table-condensed"
           cellspacing="0">
        <thead>
        <tr>
            <th class="product-name"><?php esc_html_e('Product', 'woocommerce'); ?></th>
            <th class="product-price"><?php esc_html_e('Price', 'woocommerce'); ?></th>
            <th class="product-quantity"><?php esc_html_e('Quantity', 'woocommerce'); ?></th>
            <th class="product-subtotal"><?php esc_html_e('Subtotal', 'woocommerce'); ?></th>
            <th class="product-remove">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php do_action('woocommerce_before_cart_contents'); ?>

        <?php
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                ?>
                <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                    <td class="product-name" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                        <div class="thumb_cart">
                            <?php
                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
                            echo $thumbnail;
                            //                            if (!$product_permalink) {
                            //                                echo $thumbnail; // PHPCS: XSS ok.
                            //                            } else {
                            //                                printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail); // PHPCS: XSS ok.
                            //                            }
                            ?>
                        </div>

                        <span class="item_cart">
                                <?php
                                if (!$product_permalink) {
                                    echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
                                } else {
                                    echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s"><h4>%s</h4></a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
                                }

                                do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                // Meta data.
                                echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

                                // Backorder notification.
                                if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                    echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
                                }
                                ?>
                            </span>
                    </td>

                    <td class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                        <?php
                        echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                        ?>
                    </td>

                    <td class="product-quantity"
                        data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                        <div class="numbers-row">
                            <?php
                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else {
                                $product_quantity = woocommerce_quantity_input(
                                    array(
                                        'input_name' => "cart[{$cart_item_key}][qty]",
                                        'input_value' => $cart_item['quantity'],
                                        'max_value' => $_product->get_max_purchase_quantity(),
                                        'min_value' => '0',
                                        'product_name' => $_product->get_name(),
                                        'class' => 'form-control text-center'
                                    ),
                                    $_product,
                                    false
                                );
                            }

                            echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.
                            ?>
                        </div>
                    </td>

                    <td class="product-subtotal"
                        data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>">
                        <?php
                        echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                        ?>
                    </td>
                    <td class="product-remove options">
                        <a href="<?= wc_get_cart_remove_url($cart_item_key) ?>"
                           data-product_id="<?= $product_id ?>"
                           data-product_sku="<?= $_product->get_sku() ?>" aria-label="Remove this item">
                            <i class="ti-trash"></i>
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>

        <?php do_action('woocommerce_cart_contents'); ?>

        <?php do_action('woocommerce_after_cart_contents'); ?>
        </tbody>
    </table>

    <div class="row add_top_30 flex-sm-row-reverse cart_actions">
        <div class="actions col-sm-4 text-right">
            <button type="submit" class="btn_1 gray" name="update_cart"
                    value="Update cart">Update cart
            </button>
        </div>
        <?php if (wc_coupons_enabled()) { ?>
            <div class="col-sm-8">
                <div class="coupon apply-coupon">
                    <div class="form-group form-inline">
                        <input
                                type="text" name="coupon_code" class="input-text form-control" id="coupon_code" value=""
                                placeholder="Coupon code"/>
                        <button type="submit" class="btn_1 outline" name="apply_coupon"
                                value="Apply coupon">Apply coupon
                        </button>
                        <?php do_action('woocommerce_cart_coupon'); ?>
                    </div>
                </div>
            </div>

        <?php } ?>



        <?php do_action('woocommerce_cart_actions'); ?>

        <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
    </div>
    <?php do_action('woocommerce_after_cart_table'); ?>
</form>

<?php do_action('woocommerce_before_cart_collaterals'); ?>

<div class="cart-collaterals">
    <?php
    /**
     * Cart collaterals hook.
     *
     * @hooked woocommerce_cross_sell_display
     * @hooked woocommerce_cart_totals - 10
     */
    do_action('woocommerce_cart_collaterals');
    ?>
</div>
<?php do_action('woocommerce_after_cart'); ?>
