<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
?>
<?php
if (is_shop() || is_archive()):
    ?>
    <div class="col-6 col-md-4 col-xl-3">
<?php
endif;
?>
    <div class="grid_item">
        <figure>
            <?php woocommerce_show_product_loop_sale_flash() ?>
            <a href="<?= $product->get_permalink() ?>">
                <?php
                /**
                 * Hook: woocommerce_before_shop_loop_item_title.
                 *
                 * @hooked woocommerce_show_product_loop_sale_flash - 10
                 * @hooked woocommerce_template_loop_product_thumbnail - 10
                 */
                //                do_action( 'woocommerce_before_shop_loop_item_title' );
                woocommerce_template_loop_product_thumbnail();
                ?>
            </a>
        </figure>
        <a href="<?= $product->get_permalink() ?>">
            <?php
            /**
             * Hook: woocommerce_shop_loop_item_title.
             *
             * @hooked woocommerce_template_loop_product_title - 10
             */
            do_action('woocommerce_shop_loop_item_title');
            ?>
        </a>
        <div class="price_box">
            <?php
            /**
             * Hook: woocommerce_after_shop_loop_item_title.
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            do_action('woocommerce_after_shop_loop_item_title');
            ?>
        </div>
        <ul>
            <li><?php echo do_shortcode("[ti_wishlists_addtowishlist]"); ?></li>
            <li><a href="javascript:void(0);" class="tooltip-1 quick-view" data-placement="left" title="Quick View"
                   data-toggle="modal"
                   data-target="#quick-view-popup" data-product-id="<?= $product->get_id() ?>"><i
                            class="ti-zoom-in"></i><span>Quick View</span></a></li>
            <li><?php woocommerce_template_loop_add_to_cart(); ?></li>
        </ul>
    </div>
<?php
if (is_shop() || is_archive()):
    ?>
    </div>
<?php
endif;