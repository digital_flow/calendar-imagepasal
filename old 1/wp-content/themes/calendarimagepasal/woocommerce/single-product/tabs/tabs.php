<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters('woocommerce_product_tabs', array());

if (!empty($product_tabs)) : ?>

    <div class="tabs_product">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <?php foreach ($product_tabs as $key => $product_tab) : ?>
                    <li class="nav-item">
                        <a href="#pane-<?php echo esc_attr($key); ?>" class="nav-link"
                           id="tab-<?php echo esc_attr($key); ?>"
                           role="tab" aria-controls="tab-<?php echo esc_attr($key); ?>" data-toggle="tab">
                            <?php echo wp_kses_post(apply_filters('woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key)); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <!-- /tabs_product -->
    <div class="tab_content_wrapper">
        <div class="container">
            <div class="tab-content" role="tablist">
                <?php foreach ($product_tabs

                               as $key => $product_tab) : ?>
                    <div class="card tab-pane fade show"
                         id="pane-<?php echo esc_attr($key); ?>" role="tabpanel"
                         aria-labelledby="tab-title-<?php echo esc_attr($key); ?>">
                        <div class="collapse" id="collapse-<?php echo esc_attr($key); ?>" role="tabpanel">
                            <div class="card-body">
                                <div class="justify-content-between">
                                    <?php
                                    if (isset($product_tab['callback'])) {
                                        call_user_func($product_tab['callback'], $key, $product_tab);
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- /TAB A -->
                <?php endforeach; ?>
            </div><!-- /tab-content -->
        </div><!-- /container -->
    </div><!-- /tab_content_wrapper -->
    <?php do_action('woocommerce_product_after_tabs'); ?>


<?php endif; ?>

