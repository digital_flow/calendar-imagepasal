        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap py-0">

                <!-- 1. Section Why Choose Us
                ============================================= -->
               <div class="section bg-transparent  my-0">
                    <div class="container text-center mw-md">

                        <h2 class="display-4 fw-normal"><?php the_sub_field('heading'); ?></h2>

                        <div class="clear"></div>

                        <!-- Features Area -->
                        <div class="row col-mb-50 mb-0 mt-5 justify-content-center">

                             <?php  while (have_rows('features')):
                                the_row();

                                ?>


                          <div class="col-sm-6 col-lg-4">
                                <div class="feature-box fbox-center fbox-dark fbox-plain">
                                    <div class="fbox-icon">
                                      <?php the_sub_field('icon'); ?>
                                    </div>
                                    <div class="fbox-content">
                                        <h2 class="nott fw-medium h4 mb-4"><?php the_sub_field('title'); ?></h2>
                                        <p class=""><?php the_sub_field('paragraph'); ?></p>
                                    </div>
                                </div>
                            </div>

                      <?php
                            endwhile;
                            ?>


                        </div>


                    </div>
                </div> <!-- Section End -->