<?php

get_header();

while (have_posts()):
    the_post();

    if (get_the_content()) {
        if (!is_home()) {
            ?>
            <div class="container margin_30">
            <div class="page_header">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?= get_home_url() ?>">Home</a></li>
                        <li><?php the_title() ?></li>
                    </ul>
                </div>
                <h1><?php the_title() ?></h1>
            </div>
            <!-- /page_header -->
            <?php
        } else {
            the_title();
        }
        ?>
        <div class="container">
            <?php
            if (!is_user_logged_in() && is_page('checkout') && get_option('woocommerce_enable_checkout_login_reminder') != "no"):
                ?>
                <a href="javascript:void(0)" class="h6 login-link" id="checkout-login">Login</a>
            <?php
            endif;
            ?>
            <?php the_content(); ?>
        </div>
        <?php
    }

endwhile;

if (have_rows('sections')) {
    while (have_rows('sections')):
        the_row();
        get_template_part('/partials/flexible-content/' . get_row_layout());
    endwhile;
}

get_footer();