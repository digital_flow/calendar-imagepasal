 <!-- 4. Section Explore your Home & Office
                ============================================= -->
                <div class="container-fluid">

                    <div class="row align-items-lg-center col-mb-30">

                        <!-- Image -->
                        <div class="col-xl-8 col-lg-6 px-lg-0 min-vh-50 min-vh-lg-75" style="background: url('<?php the_sub_field('image'); ?>') no-repeat center center; background-size: cover;">
                        </div>

                        <!-- Content -->
                        <div class="col-xl-4 col-lg-6 px-lg-5 py-5">
                            <h3 class="h1 mb-4 fw-normal"><?php the_sub_field('heading'); ?></h3>
                            <p><?php the_sub_field('paragraph'); ?></p>
                            <a href="<?php the_sub_field('button_link'); ?>" class="button button-border m-0 button-dark border-width-1 border-default h-bg-primary"><?php the_sub_field('button_text'); ?> <i class="icon-long-arrow-right"></i></a>
                        </div>
                    </div>

                </div>
                <!-- Section End -->