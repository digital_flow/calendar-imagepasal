 <!-- 3. Section new Arrivals
                ============================================= -->
                <div class="section custom-bg mt-3 mb-0" style="--custom-bg: #F3F3ED; padding: 100px 0;">
                    <div class="container">

                        <!-- Shop
                        ============================================= -->
                        <div id="shop" class="shop row gutter-30 col-mb-30 mt-3">

                            <!-- Title -->
                            <div class="col-xl-3 col-lg-6">
                                <h3 class="mb-4 fw-normal h1"><?php the_sub_field('heading'); ?>
                                </h3>
                                    <p class="op-07 mb-4"><?php the_sub_field('paragrpah'); ?></p>
                                <a href="<?php the_sub_field('button_link'); ?>" class="button  nott ls0 fw-normal button-primary "><?php the_sub_field('button_text'); ?></a>

                            </div>

                                <?php

                        $args = [
                            'fields'         => 'ids',
                            'post_type'      => 'product',
                            'status'         => 'publish',
                            'posts_per_page' => '7',
                            'post__in'       => wc_get_featured_product_ids()
                        ];

                        // var_dump(wc_get_featured_product_ids());

                        $featured_products = get_posts( $args );

                            foreach ( $featured_products as $product_id ):
//                                 $product = wc_get_product($product_id);
                                ?>
                            <?php get_single_product_html($product_id);
                            
                            ?>

                          

                           <?php
                            endforeach;
                            ?>

                            <!-- Product 6 -->
                            <!-- <div class="product col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="grid-inner">
                                    <div class="product-image">
                                        <a href="demo-forum-single.html"><img src="demos/furniture/images/shop/8.jpg" alt="White Cuddle Chair with Cusions"></a>
                                        <a href="demo-forum-single.html"><img src="demos/furniture/images/shop/8-1.jpg" alt="White Cuddle Chair with Cusions"></a>
                                        <div class="bg-overlay">
                                            <div class="bg-overlay-content align-items-end justify-content-between" data-hover-animate="fadeIn" data-hover-speed="400">
                                                <a href="#" class="btn btn-light me-2"><i class="icon-line-shopping-cart"></i></a>
                                                <a href="demos/furniture/ajax/quick-view.html" class="btn btn-light" data-lightbox="ajax"><i class="icon-line-expand"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-desc">
                                        <div class="product-title mb-0">
                                            <h4 class="mb-0"><a class="fw-medium" href="demo-forum-single.html">White Cuddle Chair with Cusions</a></h4>
                                        </div>
                                        <h5 class="product-price fw-normal">$29.99</h5>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                        <!-- #shop end -->

                    </div>
                </div>
                <!-- Section End -->