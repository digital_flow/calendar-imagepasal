<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version     3.6.0
 */


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (is_user_logged_in()) {
    return;
}

?>
<form class="woocommerce-form woocommerce-form-login login form_container d-none"
      method="post" <?php echo ($hidden) ? 'style="display:none;"' : ''; ?>>

    <?php do_action('woocommerce_login_form_start'); ?>

    <?php echo ($message) ? wpautop(wptexturize($message)) : ''; // @codingStandardsIgnoreLine ?>
    <div class="row">
        <div class="col form-group">
            <input type="text" class="input-text form-control" name="username" id="username"
                   placeholder="Username or email"
                   autocomplete="username"/>
        </div>
        <div class="col form-group">
            <input class="input-text form-control" type="password" name="password" placeholder="Password" id="password"
                   autocomplete="current-password"/>
        </div>
    </div>
    <div class="clear"></div>

    <?php do_action('woocommerce_login_form'); ?>

    <div class="clearfix add_bottom_15">
        <div class="checkboxes float-left">
            <label class="container_check">Remember me
                <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme"
                       type="checkbox"
                       id="rememberme" value="forever"/><span class="checkmark"></span>
            </label>
        </div>
        <div class="lost_password float-right">
            <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'woocommerce'); ?></a>
        </div>
    </div>

    <div class="text-center">
        <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
        <input type="hidden" name="redirect" value="<?php echo esc_url($redirect) ?>"/>
        <button type="submit" class="woocommerce-form-login__submit btn_1 full-width" name="login"
                value="<?php esc_attr_e('Login', 'woocommerce'); ?>"><?php esc_html_e('Login', 'woocommerce'); ?></button>
        <div class="clear"></div>
    </div>
    <?php do_action('woocommerce_login_form_end'); ?>

</form>