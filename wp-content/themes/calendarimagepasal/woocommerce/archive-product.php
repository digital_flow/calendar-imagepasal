<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header();

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );

?>


        <!-- Content
		============================================= -->
        <section id="content">
            <div class="content-wrap">

                <div class="container">

                    <div class="row justify-content-between align-items-center mb-4">
                        <div class="col-auto ">
                            <h3 class="fw-medium h3">
<?php
     
     $title = wp_title('&raquo;',FALSE);
     preg_match('/[^ ]*$/', $title, $results);
    $last_word = $results[0];
//   echo $last_word;
   global $wp_query;
   echo ucfirst($wp_query->get_queried_object()->name);
     ?>
			
			<?php echo single_post_title(); ?>
                            </h3>
                        </div>

                        <div class="col-auto d-flex">
							<div class="sortbuttons">
                              <select name="" id=""class="select " onchange="javascript:handleSelect(this)">
								  <?php $orderby = 'name';
                                        $order = 'asc';
                                        $cat_args = array(
                                            'orderby' => $orderby,
                                            'order' => $order,
                                            'hide_empty' => true,
											'exclude' => array( )
                                        );
                                        $product_categories = get_terms('product_cat', $cat_args);
// 									var_dump($product_categories); 
                                        ?>
								  
                               <option value="">Choose Category</option>
								
							 <?php
								 foreach($product_categories as $cat):
								  var_dump($cat);
											if($cat->parent == 0) {
												 $category_id = $cat->term_id; 
												
												
													
												
									 ?>
								  
								  
													
												<?php if($cat->name != "Uncategorized" ){  ?>
								 <option value="<?php echo get_term_link($cat); ?>" > <?php echo $cat->name; ?> </option>
								  
								  	<?php }
												
												$child_args = array(
              'taxonomy' => 'product_cat',
              'hide_empty' => true,
              'parent'   => $cat->term_id
          );
  $child_product_cats = get_terms( $child_args );
  foreach ($child_product_cats as $child_product_cat)
  { 
	  ?>
								  <?php if($cat->name != "Products"  ){  ?>
								  		 <option value="<?php echo get_term_link($child_product_cat); ?>">    &nbsp; &nbsp; -  <?php echo $child_product_cat->name; ?> </option>
                              
								  											<?php }

  }
												
												
												
												}
								 
							
										
 endforeach; ?>   
                              </select>
                            </div>
							
							<script type="text/javascript">
  function handleSelect(elm)
  {
// 	  console.log(elm.value);
     window.location = elm.value;
  }
</script>
								  
								  
								  
                           

                            <div class="sortbuttons ms-2" >
								 <?php woocommerce_catalog_ordering(); ?>
<!--                                 <button class="button button-border ml-2 m-0 button-dark border-width-1 border-default nott ls0 fw-normal dropdown-toggle" type="button" id="sortButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort by default</button>
                                <div class="dropdown-menu py-0 border-default rounded-0" aria-labelledby="sortButton">
                                    <a class="dropdown-item" href="#">Featured</a>
                                    <a class="dropdown-item" href="#">Popular</a>
                                    <a class="dropdown-item" href="#">Price: Low to High</a>
                                    <a class="dropdown-item" href="#">Price: High to Low</a>
                                    <a class="dropdown-item" href="#">Oldest to Newest</a>
                                    <a class="dropdown-item" href="#">Newest to Oldest</a>
                                    <a class="dropdown-item" href="#">A-Z</a>
                                    <a class="dropdown-item" href="#">Z-A</a>
                                </div> -->
                            </div>

                        </div>
                    </div>

                    <!-- Shop
					============================================= -->
                    <div id="shop" class="shop row col-mb-30 mt-3">

						  <?php

            if (woocommerce_product_loop()) {

            /**
             * Hook: woocommerce_before_shop_loop.
             *
             * @hooked woocommerce_output_all_notices - 10
             * @hooked woocommerce_result_count - 20
             * @hooked woocommerce_catalog_ordering - 30
             */
            //    do_action('woocommerce_before_shop_loop');
            ?>

                    <?php

                    woocommerce_product_loop_start();

                    if (wc_get_loop_prop('total')) {
                        while (have_posts()) {
                            the_post();

                            /**
                             * Hook: woocommerce_shop_loop.
                             */
                            do_action('woocommerce_shop_loop');

                            wc_get_template_part('content', 'product');
                        }
                    }

                    woocommerce_product_loop_end();
                    ?>

                <?php

                /**
                 * Hook: woocommerce_after_shop_loop.
                 *
                 * @hooked woocommerce_pagination - 10
                 */
                do_action('woocommerce_after_shop_loop');
                } else {
                    /**
                     * Hook: woocommerce_no_products_found.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action('woocommerce_no_products_found');
                }
                ?>
				
				
						
                       

                   

                    </div>
                    <!-- #shop end -->

<!--                     <div class="clear"></div>

                    <nav aria-label="Page navigation ">
                        <ul class="pagination justify-content-center mt-4">
                            <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav> -->

                </div>

            </div>
        </section>
        <!-- #content end -->

<?php

get_footer();
			
