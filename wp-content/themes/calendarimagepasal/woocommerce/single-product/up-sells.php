<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
    exit;
}

if ($upsells) : ?>
    <div class="col-lg-6 border-right pr-5" id="upsell-products">
        <div class="b-section_title">
            <h4 class="text-uppercase">
                You May Also Like
            </h4>
        </div>

        <div class="b-products b-product_grid b-product_grid_four">
            <div class="clearfix owl-carousel owl-theme" id="b-like_products">
                <?php foreach ($upsells as $upsell) : ?>
                    <div>
                        <?php
                        $post_object = get_post($upsell->get_id());

                        setup_postdata($GLOBALS['post'] =& $post_object);

                        wc_get_template_part('content', 'product'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif;

wp_reset_postdata();
