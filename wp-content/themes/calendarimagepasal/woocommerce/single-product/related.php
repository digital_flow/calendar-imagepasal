<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.9.0
 */

if (!defined('ABSPATH')) {
    exit;
}

if ($related_products) : ?>



                    <div class="section bg-transparent my-0 py-5">
                        <div class="mw-sm mx-auto text-center px-4 px-md-0">
						<h2 class="fw-medium mb-3">Other Product</h2>
                        </div>
                        <div class="container">
						<div class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-md="2" data-items-lg="3" data-items-xl="4">
							
							 <?php foreach ($related_products as $related_product) : ?>
							 <?php
                    $post_object = get_post($related_product->get_id());

                    setup_postdata($GLOBALS['post'] =& $post_object); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

                    //wc_get_template_part('content', 'product');
                    ?>
                    <div class="oc-item">
                                <div class="product">
                                    <div class="grid-inner">
                                        <div class="product-image">
                                            <a href="<?= $related_product->get_permalink() ?>"><?php $image = wp_get_attachment_image_url($related_product->get_image_id(), 'medium'); ?><img src="<?php echo $image; ?>" alt="<?php echo $related_product->get_name(); ?>" style="width:300px; height:215px; object-fit: cover;"></a>
                                            <div class="bg-overlay d-none">
                                                <div class="bg-overlay-content align-items-end justify-content-between animated fadeOut" data-hover-animate="fadeIn" data-hover-speed="400" style="animation-duration: 400ms;">
                                                    <a href="" class="btn btn-light me-2"><i class="icon-line-shopping-cart"></i></a>
                                                    <a href="demos/furniture/ajax/quick-view.html" class="btn btn-light" data-lightbox="ajax"><i class="icon-line-expand"></i></a>
                                                </div>
                                            </div>
                                        </div>
										<div class="product-desc">
											<div class="product-title mb-2"><h4 class="mb-0"><a class="fw-medium" href="<?= $related_product->get_permalink() ?>"><?php echo $related_product->get_name(); ?></a></h4></div>
											<a href="<?= $related_product->get_permalink() ?>" class="inquiry-link">Inquiry Now <i class="icon-long-arrow-right"></i></a>
										</div>
										
                                        
                                    </div>
                                </div>
                            </div>
                            
                             <?php endforeach; ?>
    						</div>
                        </div>
                    </div>
           


       

<?php
endif;

wp_reset_postdata();


