<?php

/**
 * Adds webp filetype to allowed mimes
 *
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 *
 * @param array $mimes Mime types keyed by the file extension regex corresponding to
 *                     those types. 'swf' and 'exe' removed from full list. 'htm|html' also
 *                     removed depending on '$user' capabilities.
 *
 * @return array
 */
function wpse_mime_types($mimes)
{
    if (is_user_logged_in() && current_user_can('administrator')) {
        $mimes['svg'] = 'image/svg+xml';
        $mimes['svgz'] = 'image/svg+xml';
        $mimes['webp'] = 'image/webp';
    }

    return $mimes;
}

add_filter('upload_mimes', 'wpse_mime_types');

//remove contact form 7 js and css
add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

/**
 * update wordpress tag cloud args
 *
 * @param $args
 *
 * @return array
 */
function aatti_widget_tag_cloud_args($args)
{

    $my_args = array(
        'smallest' => '14',
        'unit' => 'px'
    );
    $args = wp_parse_args($args, $my_args);

    return $args;
}

add_filter('widget_tag_cloud_args', 'aatti_widget_tag_cloud_args');


// to add class on nav bar "li" element
function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);


// to add class on nav bar "a" element
function add_additional_class_on_a($classes, $item, $args)
{
    if (isset($args->add_a_class)) {
        $classes['class'] = $args->add_a_class;
    }
    return $classes;
}

add_filter('nav_menu_link_attributes', 'add_additional_class_on_a', 1, 3);
