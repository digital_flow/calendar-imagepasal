<?php 

function calendar_support(){
	//dynamic title support
	add_theme_support('title-tag');
	add_theme_support('custom-logo');
	add_theme_support('post-thumbnails');
	// add_image_size('category-thumb', 265, 260, true);
}
add_action('after_setup_theme','calendar_support');


function register_menus(){
	register_nav_menus([
		'primary' => 'Primary Menu',
		'footer' => 'Footer Menu',
		'popular' => 'Popular Category',
		'header' => 'Header Menu'
	]);
}
add_action('init','register_menus');



function calendar_register_styles(){
	$version = wp_get_theme()->get('Version');
	wp_enqueue_style('calendar-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", array(), '4.4.1', 'all');

	// wp_enqueue_style('calendar-fontawesome', get_template_directory_uri() . '/assets/css/font-awesome.css', array(), '$version', 'all' );

	

	wp_enqueue_style('calendar-swiper', get_template_directory_uri() . '/assets/css/swiper.css', array(), '$version', 'all' );

	wp_enqueue_style('calendar-font-icons', get_template_directory_uri() . '/assets/css/font-icons.css', array(), '$version', 'all' );

	wp_enqueue_style('calendar-font-icons', get_template_directory_uri() . '/assets/css/font-icons.css', array(), '$version', 'all' );

	wp_enqueue_style('calendar-animate', get_template_directory_uri() . '/assets/css/animate.css', array(), '$version', 'all' );


	wp_enqueue_style('calendar-magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), '$version', 'all' );

	// wp_enqueue_style('calendar-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), '$version', 'all' );

	// wp_enqueue_style('calendar-style', get_template_directory_uri() . '/assets/css/style.css', array('calendar-bootstrap'), '$version', 'all');

	wp_enqueue_style('calendar-style', get_template_directory_uri() . '/assets/css/style.css', array(), '$version', 'all');

	wp_enqueue_style('calendar-furniture', get_template_directory_uri() . '/assets/demos/furniture/furniture.css', array(), '$version', 'all' );

	wp_enqueue_style('calendar-furniture-font', get_template_directory_uri() . '/assets/demos/furniture/css/fonts.css', array(), '$version', 'all' );

	// wp_enqueue_style('calendar-google-fonts-gstatic', "//fonts.gstatic.com/", array(), '$version', 'all' );

	// wp_enqueue_style('calendar-google-fonts-roboto', "fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;family=Zilla+Slab:wght@400;500&amp;display=swap", array(), '$version', 'all' );
}
add_action('wp_enqueue_scripts', 'calendar_register_styles');




function calendar_register_scripts(){
	$version = wp_get_theme()->get('Version');

	// wp_enqueue_scripts('calendar-jquery', "https://code.jquery.com/jquery-3.4.1.slim.min.js", array(), '3.4.1', true);

	wp_enqueue_script('calendar-jquery',   get_template_directory_uri() . '/assets/js/jquery.js', array(), '$version', true);
	
	wp_enqueue_script('calendar-plugins',   get_template_directory_uri() . '/assets/js/plugins.min.js', array(), '$version', true);

	wp_enqueue_script('calendar-functions',   get_template_directory_uri() . '/assets/js/functions.js', array(), '$version', true);


}
add_action('wp_enqueue_scripts', 'calendar_register_scripts');

